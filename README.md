# @umi-material/EduBasicTable

basic table for steam edu platform

## Usage

```sh
umi block https://github.com/git@gitlab.com:VicentYc/edubasictable.git/tree/master/EduBasicTable
```

## LICENSE

MIT
