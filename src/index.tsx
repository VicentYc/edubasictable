import Form, { FormComponentProps } from 'antd/lib/form';
import React, { Component } from 'react';

import { Card, Button, Switch } from 'antd';
import { ColumnProps } from 'antd/lib/table';
import { connect } from 'dva';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Action, Dispatch } from 'redux';
import { formatMessage } from 'umi-plugin-react/locale';
import { StateType } from './model';
import StandardTable from './components/StandardTable';
import styles from './style.less';
import { TableListItem, TableListPagination } from './data.d';

export interface BasicTableListProps extends FormComponentProps {
  dispatch: Dispatch<
    Action<'roleManagement/fetch' | 'roleManagement/add' | 'roleManagement/update'>
  >;
  roleManagement: StateType;
}

@connect(
  ({
    roleManagement,
    loading,
  }: {
    roleManagement: StateType;
    loading: {
      effects: { [key: string]: boolean };
    };
  }) => ({
    roleManagement,
    loading: loading.effects['roleManagement/fetch'],
  }),
)
class BasicTableList extends Component<BasicTableListProps, any> {
  columns: ColumnProps<TableListItem>[] = [
    {
      title: '角色名称',
      dataIndex: 'name',
      width: '30%',
    },
    {
      title: '角色备注',
      dataIndex: 'remark',
      width: '30%',
    },
    {
      title: '是否启用角色',
      dataIndex: 'enable',
      align: 'center',
      width: '20%',
      render: (text, record: TableListItem) => {
        const { enable } = record;
        return (
          <Switch
            checked={enable}
            checkedChildren={formatMessage({
              id: 'platform-setting.role-management.table-item.statu.yes',
            })}
            unCheckedChildren={formatMessage({
              id: 'platform-setting.role-management.table-item.statu.no',
            })}
            onClick={() => {}}
          />
        );
      },
    },
    {
      title: '操作',
      align: 'center',
      width: '20%',
      render: () => (
        <Button type="link" onClick={() => {}}>
          {formatMessage({ id: 'platform-setting.role-management.table-item.edit' })}
        </Button>
      ),
    },
  ];

  componentDidMount() {
    const { dispatch, roleManagement } = this.props;
    const { data } = roleManagement;
    const { pagination } = data;
    dispatch({
      type: 'roleManagement/fetch',
      payload: pagination,
    });
  }

  handleStandardTableChange = (pagination: Partial<TableListPagination>) => {
    const { dispatch } = this.props;
    const { current, pageSize } = pagination;
    const params: Partial<TableListPagination> = {
      current,
      pageSize,
    };
    dispatch({
      type: 'roleManagement/fetch',
      payload: params,
    });
  };

  renderSimpleRow = () => (
    <Form.Item>
      <Button type="primary" className={styles.createBtn} onClick={() => {}}>
        {formatMessage({ id: 'platform-setting.role-management.add-role' })}
      </Button>
    </Form.Item>
  );

  render() {
    const { roleManagement } = this.props;
    const { data } = roleManagement;

    return (
      <PageHeaderWrapper title={false}>
        <Card className="wrapperCard" bordered={false}>
          {this.renderSimpleRow()}
          <StandardTable loading={false} data={data} columns={this.columns} onChange={() => {}} />
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default Form.create<BasicTableListProps>()(BasicTableList);
