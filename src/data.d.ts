export interface TableListItem {
  id: number;
  name: string;
  remark: string;
  enable: boolean;
}

export interface SaveDataType {
  data: BasicTableData;
}

export interface BasicTableData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface TableListPagination {
  total?: number;
  pageSize: number;
  current: number;
}
