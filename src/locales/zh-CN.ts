export default {
  'platform-setting.role-management.add-role': '创建角色',
  'platform-setting.role-management.model.fetch.error': '获取数据失败',
  'platform-setting.role-management.table-item.statu.yes': '是',
  'platform-setting.role-management.table-item.statu.no': '否',
  'platform-setting.role-management.table-item.edit': '编辑',
};
