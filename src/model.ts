import { Effect } from 'dva';
import { Reducer } from 'redux';
import { BasicTableData, SaveDataType } from './data.d';

export interface StateType {
  data: BasicTableData;
}

export interface ModelType {
  namespace: string;
  state: StateType;
  effects: {
    fetch: Effect;
  };
  reducers: {
    save: Reducer<SaveDataType>;
  };
}

const Model: ModelType = {
  namespace: 'roleManagement',
  state: {
    data: {
      list: [],
      pagination: {
        current: 1,
        pageSize: 30,
        total: 0,
      },
    },
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      //   const response = yield call(queryRoles, payload);
      //   if (response && response.code === 0) {
      //     yield put({
      //       type: 'save',
      //       payload: response.data,
      //     });
      //   } else {
      //     message.error(formatMessage({ id: 'platform-setting.staff-management.model.fetch.error' }));
      //   }
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        data: payload,
      };
    },
  },
};
export default Model;
